// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebaseconfig: {
    apiKey: 'AIzaSyACFCMfKKGmGXqwvmO4B9gu5HHHmX4JrE8',
    authDomain: 'vacapp-d18ab.firebaseapp.com',
    databaseURL: 'https://vacapp-d18ab.firebaseio.com',
    projectId: 'vacapp-d18ab',
    storageBucket: '',
    messagingSenderId: '1052953305822',
    appId: '1:1052953305822:web:8a945c46dff76794ec984f',
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
