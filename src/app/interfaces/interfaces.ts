
export interface Vaca {
    id?: string;
    folio?: string;
    edad?: number;
    raza?: string;
    madre?: string;
    padre?: string;
    estatus?: boolean;
    produccion?: Ordenada[];
    clinico?: Medico[];
}

export interface Ordenada {
    turno?: string;
    cantidad?: number;
    fecha?: string;
    hora?: string;
}

export interface Medico {
    vacuna?: string;
    enfermedad?: string;
    fecha?: string;
    num_partos?: number;
    inseminacion?: number;
}