import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HeaderComponent } from './header/header.component';
import { IonicModule } from '@ionic/angular';
import { OrdenadaComponent } from './ordenada/ordenada.component';
import { InformacionComponent } from './informacion/informacion.component';
import { FormsModule } from '@angular/forms';
import { MedicoComponent } from './medico/medico.component';

@NgModule({
  declarations: [
    HeaderComponent,
    OrdenadaComponent,
    InformacionComponent,
    MedicoComponent
  ],
  exports: [
    HeaderComponent,
    OrdenadaComponent,
    InformacionComponent,
    MedicoComponent
  ],
  imports: [
    CommonModule,
    IonicModule,
    FormsModule
  ]
})
export class ComponentsModule { }
