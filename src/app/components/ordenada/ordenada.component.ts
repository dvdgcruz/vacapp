import { Component, OnInit, Input } from '@angular/core';
import { ModalController } from '@ionic/angular';

@Component({
  selector: 'app-ordenada',
  templateUrl: './ordenada.component.html',
  styleUrls: ['./ordenada.component.scss'],
})
export class OrdenadaComponent implements OnInit {

  @Input() ordenada;

  constructor(private modal: ModalController) { }

  ngOnInit() {}

  guardar() {
    this.modal.dismiss({
      ordenada: this.ordenada
    });
  }

  salir() {
    this.modal.dismiss();
  }

}
