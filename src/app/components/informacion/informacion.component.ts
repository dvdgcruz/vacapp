import { Component, OnInit, Input } from '@angular/core';
import { ModalController } from '@ionic/angular';

@Component({
  selector: 'app-informacion',
  templateUrl: './informacion.component.html',
  styleUrls: ['./informacion.component.scss'],
})
export class InformacionComponent implements OnInit {

  @Input() vaca;

  constructor(private modal: ModalController) { }

  ngOnInit() {}

  guardar() {
    this.modal.dismiss({
      vaca: this.vaca
    });
  }
  cancelar() {
    this.modal.dismiss();
  }

}
