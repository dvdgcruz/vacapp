import { Component, OnInit, Input } from '@angular/core';
import { ModalController } from '@ionic/angular';

@Component({
  selector: 'app-medico',
  templateUrl: './medico.component.html',
  styleUrls: ['./medico.component.scss'],
})
export class MedicoComponent implements OnInit {

  @Input() medico;

  constructor(private modal: ModalController) { }

  ngOnInit() {}

  guardar() {
    this.modal.dismiss({
      medico: this.medico
    });
  }

  salir() {
    this.modal.dismiss();
  }
}
