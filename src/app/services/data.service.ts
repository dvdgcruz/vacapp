import { Injectable } from '@angular/core';

import { AngularFirestoreCollection, AngularFirestoreDocument, AngularFirestore } from '@angular/fire/firestore';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { UiService } from './ui.service';
import { Storage } from '@ionic/storage';
import { NavController } from '@ionic/angular';
import { Vaca } from '../interfaces/interfaces';


@Injectable({
  providedIn: 'root'
})
export class DataService {

  col: string;
  private collection: AngularFirestoreCollection<any>;
  private array: Observable<any[]>;
  document: AngularFirestoreDocument<any>;
  object: Observable<any>;

  constructor( 
    private afs: AngularFirestore,
    private uiservice: UiService,
    private navCtrl: NavController,
    private localStorage: Storage
    ) {}

  init() {
    this.col = 'Vacas Menu';
    this.collection = this.afs.collection<any>(`${ this.col }`);
    this.array = this.collection.valueChanges();
  }

  guardaVaca( vaca: Vaca) {
    this.localStorage.set('vaca', vaca);
  }

  async leerVaca(){
    let vaca: Vaca;
    
    await this.localStorage.get('vaca').then( result => {
      if( result && result.folio){
        vaca = result;
      }
    });
    return vaca;
  }

  eliminaVaca() {
    this.localStorage.remove('vaca');
  }

  getAll() {
    return this.array = this.collection.snapshotChanges()
    .pipe( map( cambios => {
      return cambios.map( accion => {
        const data = accion.payload.doc.data() as any;
        data.id = accion.payload.doc.id;
        return data;
      });
    }));
  }

  getOne(idobject: string) {
    this.document = this.afs.doc<any>(`${ this.col }/${idobject}`);
    return this.object = this.document.snapshotChanges().pipe(map(action => {
      if (action.payload.exists === false) {
        return null;
      } else {
        const data = action.payload.data() as any;
        data.id = action.payload.id;
        return data;
      }
    }));
  }

  add(object: any): void {
    this.collection.add({...object}).then( resp => {
      this.uiservice.presentToast('Se agregó correctamente');
    });
  }

  update(object: any, idobject: string): void {
    // const idobject = object.id;
    delete object.id;
    this.document = this.afs.doc<any>(`${ this.col }/${idobject}`);
    this.document.update( object ).then( resp => {
      this.uiservice.presentToast('Se guardó correctamente');
    });
  }

  delete(idobject: string): void {
    this.document = this.afs.doc<any>(`${ this.col }/${idobject}`);
    this.document.delete().then( resp => {
      this.uiservice.presentToast('Se eliminó correctamente');
    });
  }

}
