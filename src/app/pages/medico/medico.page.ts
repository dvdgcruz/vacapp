import { Component, OnInit, ViewChild } from '@angular/core';
import { DataService } from 'src/app/services/data.service';
import { Vaca, Ordenada } from 'src/app/interfaces/interfaces';
import { IonList, ModalController, NavController } from '@ionic/angular';
import { Medico } from '../../interfaces/interfaces';
import { MedicoComponent } from '../../components/medico/medico.component';
import * as _ from 'lodash';

@Component({
  selector: 'app-medico',
  templateUrl: './medico.page.html',
  styleUrls: ['./medico.page.scss'],
})
export class MedicoPage implements OnInit {

  @ViewChild('lista', {static: false}) lista: IonList;
  idVaca: string;
  vaca: Vaca = {clinico : []};
  clinico: Medico[] = [];
  nuevoMedico: Medico = {};


  constructor(
    private data: DataService,
    private modal: ModalController,
    private navCtrl: NavController
  ) { }

  ionViewWillEnter() {
    this.data.init();
    this.data.leerVaca().then( vaca => {
      if(!vaca){
        this.navCtrl.navigateRoot('home');
      } else {
        this.idVaca = vaca['id'];
        this.vaca = vaca;
        this.ordenar();
      }
    });
  }

  ngOnInit() {
  }

  ordenar() {
    this.vaca.clinico = _.orderBy(this.vaca.clinico, ['fecha', 'enfermedad'], ['desc', 'asc']);
    this.data.guardaVaca( this.vaca );
  }

  async abrirModal() {
    const modal = await this.modal.create({
      component: MedicoComponent,
      componentProps: {
        medico: this.nuevoMedico
      }
    });
    await modal.present();
    const { data } = await modal.onDidDismiss();
    // console.log(data.ordenada);
    if ( data && data.medico ) {
      console.log('id->', this.idVaca);
      this.vaca.clinico.push( data.medico );
      this.data.update( this.vaca, this.idVaca );
      this.vaca.id = this.idVaca;
      this.ordenar();
      this.nuevoMedico = {};
    }
  }
  async editar( medi: Medico, posicion: number ) {
    console.log(posicion);
    const modal = await this.modal.create({
      component: MedicoComponent,
      componentProps: {
        medico: medi
      }
    });
    await modal.present();
    const { data } = await modal.onDidDismiss();
    if ( data && data.medico ) {
      console.log(this.idVaca);
      this.vaca.clinico[posicion] = data.medico;
      this.data.update( this.vaca, this.idVaca );
      this.vaca.id = this.idVaca;
      this.data.guardaVaca( this.vaca );
    }
    this.lista.closeSlidingItems();
  }

}
