import { Component, OnInit, ViewChild } from '@angular/core';

import chartJs from 'chart.js';
import { DataService } from 'src/app/services/data.service';
import { NavController } from '@ionic/angular';
import { Vaca } from 'src/app/interfaces/interfaces';

import * as _ from 'lodash';


@Component({
  selector: 'app-graficas',
  templateUrl: './graficas.page.html',
  styleUrls: ['./graficas.page.scss'],
})
export class GraficasPage implements OnInit {

  @ViewChild('barCanvas',{static:false}) barCanvas;
  @ViewChild('lineCanvas',{static:false}) lineCanvas;

  barChart: any;
  graph: any;
  lineChart: any;
  idVaca: string;
  vaca: Vaca = {};
  vacas: Vaca[] = [];
  totales = {
    vacas: [] = [],
    cantidades: [] = []
  };

  constructor(
    private data: DataService,
    private navCtrl: NavController
  ) { }

  ngOnInit() {
    // obtener datos
    this.data.leerVaca().then( vaca => {
      if(!vaca){
        this.navCtrl.navigateRoot('home');
      } else {
        this.idVaca = vaca['id'];
        this.vaca = vaca;
      }
    });

    this.data.init();

    this.data.getAll().subscribe(resp => {
      this.vacas = resp;
      // console.log('Obteniendo vacas->', this.vacas);
      this.getTotales();
    });

    

  }

  ngAfterViewInit() {
    setTimeout( () => {
      this.barChart = this.getBarChart();
      // this.lineChart = this.getLineChart();
    }, 1000)
  }

  getChart( context, chartType, data, options? ) {
    return new chartJs( context, {
      data,
      options,
      type: chartType
    })
  }

  getTotales() {
    // this.totales.vacas = [ 'chenta', 'potra', 'torta'];
    // console.log('Leyendo vacas->', this.vacas);
    // this.totales.cantidades = [4, 5, 10];

    for (let i = 0; i < this.vacas.length; i++) {
      const element = this.vacas[i];
      let cantidadTotal = 0;

      this.totales.vacas.push( element.folio );
      if( element.produccion && element.produccion.length > 0 ) {
        element.produccion.map((ordenada) => {
          cantidadTotal += ordenada.cantidad;
        });
      }
      this.totales.cantidades.push( cantidadTotal );
    }
  }

  getBarChart() {
    
      

    this.graph = {
      // labels: ['Azul', 'Amarillo', 'Verde', 'Rojo'],
      labels: this.totales.vacas,
      datasets: [{
        label: 'Total de lts por vaca',
        // data: [12,23,15,90],
        data: this.totales.cantidades,
        backgroundColor: [
          'rgb( 20,0,255)',
          'rgb( 255,230,0)',
          'rgb( 0,255,10)',
          'rgb( 60,0,70)',
        ],
        borderWidth: 1
      }]
    };

    const options = {
      scales: {
        yAxes: [{
          ticks: {
            beginAtZero: true
          }
        }]
      },
      responsive: true,
    };

    return this.getChart( this.barCanvas.nativeElement, 'horizontalBar', this.graph, options);

  }

  getLineChart() {
    const data = {
      labels: ['Enero', 'Febrero', 'Marzo', 'Abril'],
      datasets: [{
        label: this.vaca.folio,
        fill: false,
        lineTension: 0.1,
        backgroundColor: 'rgb(0,178,255',
        borderColor: 'rgb(231,205,35)',
        borderCapStyle: 'butt',
        borderJoinStyle: 'miter',
        pointRadius: 1,
        pointHitRadius: 10,
        data: [20,15,98,4],
        scanGaps: false
      }]
    };

    return this.getChart(this.lineCanvas.nativeElement,'line',data);
  }

}
