import { Component, OnInit } from '@angular/core';
import { DataService } from 'src/app/services/data.service';
import { Vaca } from 'src/app/interfaces/interfaces';
import { NavController } from '@ionic/angular';

@Component({
  selector: 'app-registro',
  templateUrl: './registro.page.html',
  styleUrls: ['./registro.page.scss'],
})
export class RegistroPage implements OnInit {

  vaca: Vaca = {};

  constructor(private data: DataService, private navCtrl: NavController) { }

  ngOnInit() {
    this.data.init();
  }

  async guardar() {
    console.log('vaca a guardar->', this.vaca);
    
    this.data.add( this.vaca );
    await this.data.guardaVaca(this.vaca);
    this.navCtrl.navigateRoot('tabs');
  }

}
