import { Component, OnInit } from '@angular/core';
import { DataService } from 'src/app/services/data.service';
import { Vaca } from 'src/app/interfaces/interfaces';
import { InformacionComponent } from 'src/app/components/informacion/informacion.component';
import { ModalController, NavController, AlertController } from '@ionic/angular';

@Component({
  selector: 'app-informacion',
  templateUrl: './informacion.page.html',
  styleUrls: ['./informacion.page.scss'],
})
export class InformacionPage implements OnInit {

  idVaca: string;
  vaca: Vaca = {};

  constructor(
    private data: DataService,
    private modal: ModalController,
    private navCtrl: NavController,
    public alertController: AlertController
  ) { }

  ionViewWillEnter() {
    
    this.data.init();
    this.data.leerVaca().then( vaca => {
      if(!vaca){
        this.navCtrl.navigateRoot('home');
      } else {
        this.idVaca = vaca['id'];
        this.vaca = vaca;
      }
    });
  }
  
  ngOnInit() {
  }

  async editar() {
    const modal = await this.modal.create({
      component: InformacionComponent,
      componentProps: {
        vaca: this.vaca
      }
    });
    await modal.present();
    const { data } = await modal.onDidDismiss();
    if ( data ) {
      this.vaca = data.vaca;
      this.data.update( this.vaca, this.idVaca );
      this.vaca.id = this.idVaca;
      // this.crudService.add( this.producto );
    }
  }

  async eliminar() {
    const alert = await this.alertController.create({
      header: '¿Está seguro?',
      message: '<strong>Este proceso no se puede deshacer!!!</strong>',
      buttons: [
        {
          text: 'Cancelar',
          role: 'cancel',
          cssClass: 'secondary',
          handler: (blah) => {
            console.log('Confirm Cancel: blah');
          }
        }, {
          text: 'Continuar',
          handler: () => {
            console.log('Confirm Okay');
            this.data.delete(this.idVaca);
            this.navCtrl.navigateRoot('home');
          }
        }
      ]
    });

    await alert.present();
  }

}
