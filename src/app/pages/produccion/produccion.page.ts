import { Component, OnInit, ViewChild } from '@angular/core';
import { DataService } from 'src/app/services/data.service';
import { IonList, ModalController, NavController } from '@ionic/angular';
import { Ordenada, Vaca } from 'src/app/interfaces/interfaces';
import { OrdenadaComponent } from 'src/app/components/ordenada/ordenada.component';

import * as _ from 'lodash';

@Component({
  selector: 'app-produccion',
  templateUrl: './produccion.page.html',
  styleUrls: ['./produccion.page.scss'],
})
export class ProduccionPage implements OnInit {

  @ViewChild('lista', {static: false}) lista: IonList;
  idVaca: string;
  vaca: Vaca = {produccion: []};
  produccion: Ordenada[] = [];
  nuevaOrdenada: Ordenada = {};


  constructor(
    private data: DataService,
    private modal: ModalController,
    private navCtrl: NavController
  ) { }

  ionViewWillEnter() {
    this.data.init();
    this.data.leerVaca().then( vaca => {
      console.log(vaca);
      
      if(!vaca){
        this.navCtrl.navigateRoot('home');
      } else {
        this.idVaca = vaca['id'];
        this.vaca = vaca;
        this.ordenar();
      }
    });
  }

  ngOnInit() {
    
  }

  ordenar() {
    this.vaca.produccion = _.orderBy(this.vaca.produccion, ['fecha', 'turno'], ['desc', 'asc']);
    this.data.guardaVaca( this.vaca );
  }

  async abrirModal() {
    const modal = await this.modal.create({
      component: OrdenadaComponent,
      componentProps: {
        ordenada: this.nuevaOrdenada
      }
    });
    await modal.present();
    const { data } = await modal.onDidDismiss();
    // console.log(data.ordenada);
    if ( data && data.produccion ) {
      this.vaca.produccion.push( data.ordenada );
      this.data.update( this.vaca, this.idVaca );
      this.vaca.id = this.idVaca;
      this.ordenar();
      this.nuevaOrdenada = {};
    }
  }

  async editar( ord: Ordenada, posicion: number ) {
    console.log(posicion);
    const modal = await this.modal.create({
      component: OrdenadaComponent,
      componentProps: {
        ordenada: ord
      }
    });
    await modal.present();
    const { data } = await modal.onDidDismiss();
    if ( data && data.produccion ) {
      this.vaca.produccion[posicion] = data.ordenada;
      this.data.update( this.vaca, this.idVaca );
      this.vaca.id = this.idVaca;
      this.data.guardaVaca( this.vaca );
    }
    this.lista.closeSlidingItems();
  }

}
