import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { TabsPage } from './tabs.page';

const routes: Routes = [
  { path: '', redirectTo: 'produccion' },
  { path: 'home', loadChildren: '../home/home.module#HomePageModule' },
  {
    path: '',
    component: TabsPage,
    children: [
      {
        path: 'produccion',
        loadChildren: '../produccion/produccion.module#ProduccionPageModule'
      },
      {
        path: 'informacion',
        loadChildren: '../informacion/informacion.module#InformacionPageModule'
      },
      {
        path: 'medico',
        loadChildren: '../medico/medico.module#MedicoPageModule'
      },
      {
        path: 'graficas',
        loadChildren: '../graficas/graficas.module#GraficasPageModule'
      }
    ]
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild(routes)
  ],
  declarations: [TabsPage]
})
export class TabsPageModule {}
