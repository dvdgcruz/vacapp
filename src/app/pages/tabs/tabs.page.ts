import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-tabs',
  templateUrl: './tabs.page.html',
  styleUrls: ['./tabs.page.scss'],
})
export class TabsPage implements OnInit {

  folio: string;

  constructor(private actRoute: ActivatedRoute) { }

  ngOnInit() {
    this.actRoute.params.subscribe( params => {
      this.folio =  params.folio;
    });
  }

}
