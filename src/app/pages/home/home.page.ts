import { Component } from '@angular/core';
import { BarcodeScanner } from '@ionic-native/barcode-scanner/ngx';
import { DataService } from '../../services/data.service';
import { Vaca } from '../../interfaces/interfaces';
import { NavController } from '@ionic/angular';
import { Router } from '@angular/router';


@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})
export class HomePage {

  folio: string;
  vacas: Vaca[] = [];

  constructor(
    private barcodeScanner: BarcodeScanner,
    private data: DataService,
    private navCtrl: NavController,
    private router: Router
    ) {}

  ngOnInit() {
    this.data.eliminaVaca();
    this.data.init();
    this.data.getAll().subscribe(resp => {
      // console.log('Obteniendo vacas->', resp);
      this.vacas = resp;
    });
  }

  // leer() {

  //   this.folio = 'UTTI162008';
  //   let existe = false;

  //   for (let index = 0; index < this.vacas.length; index++) {
  //     const element = this.vacas[index];
  //     if ( element['folio'] === this.folio ) {
  //       existe = true;
  //       console.log('si existe, mandar a historial de produccion');
  //       this.data.guardaVaca( element );
  //       this.navCtrl.navigateRoot('tabs');
  //       break;
  //     } 
  //   }

  //   if(!existe) {
  //     console.log('no está registrada, mandar a crear nuevo');
  //     this.navCtrl.navigateRoot('registro');
  //   }
  //   // 
  // }

  ionViewWillEnter() {
    this.barcodeScanner.scan().then(barcodeData => {
      console.log('Barcode data', barcodeData);
      // comparar
      let existe = false;

      for (let index = 0; index < this.vacas.length; index++) {
        const element = this.vacas[index];
        if ( element['folio'] === barcodeData.text ) {
          existe = true;
          console.log('si existe, mandar a historial de produccion');
          this.data.guardaVaca( element );
          this.navCtrl.navigateRoot('tabs');
          break;
        } 
      }

      if(!existe) {
        console.log('no está registrada, mandar a crear nuevo');
        this.navCtrl.navigateRoot('registro');
      }
    }).catch(err => {
      console.log('Error', err);
    });
  }

}
